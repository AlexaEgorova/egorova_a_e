## Работа 1. Исследование гамма-коррекции
автор: Егорова А.Е.
дата: 21.04.2020

<!-- url: https://gitlab.com/AlexaEgorova/egorova_a_e/-/edit/master/prj.lab/lab01 -->

### Задание
1. Сгенерировать серое тестовое изображение **I_1** в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению **I_1** гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение **G_1**.
3. Сгенерировать серое тестовое изображение **I_2** в виде прямоугольника размером 768х60 пикселя со ступенчатым изменением яркости от черного к белому (от уровня 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
4. Применить  к изображению **I_2** гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение **G_2**.
5. Показать визуализацию результатов в виде одного изображения (рисунок 1).

### Результаты

![](lab01.png)
Рис. 1. Результаты работы программы (сверху вниз **I_1**, **G_1**, **I_2**, **G_2**)

### Текст программы

```cpp
#include <opencv2\opencv.hpp>
#include <iostream>

using namespace cv;

void PourWithGradient(Mat& img, int step, int color_step=1) {
  int color = 0;
  for (int i = 0; i + step < img.cols; i += step)
  {
    if (img.cols < 255) {
      img.col(i).setTo(i);
    }
    if (img.cols == 255) {
      for (int j = i; j < i + step; j++) {
        img.col(j).setTo(i);
      }
      int remainder = img.cols % step;
      for (int i = img.cols - remainder; i < img.cols; i++)
        img.col(i).setTo(255);
    }
    if (img.cols > 255) {
      int recounted_step = img.cols / 255;
      if (step < recounted_step)
        step = recounted_step;
      for (int j = i; j < i + step; j++) {
        img.col(j).setTo(color);
      }
    }
    int remainder = img.cols % step;
    for (int i = img.cols - remainder-5; i < img.cols; i++)
      img.col(i).setTo(255);
    color += color_step;
  }
}

void CorrectGammaByPixels(Mat& img, double gamma, double A=1.0) {
  for (int i = 0; i < img.cols; i++) {
    auto color = img.at<unsigned char>(0, i);
    auto normColor = color / 255.0;
    auto newColor = 255.0 * pow(normColor, gamma) * A;
    img.col(i).setTo(newColor);
  }
}

Mat CorrectGammaByMatrix(Mat& givenMat, double gamma, double A=1.0) {
  Mat convertedGivenMat;
  givenMat.convertTo(convertedGivenMat, CV_32F);
  convertedGivenMat /= 255.0;

  Mat poweredMat;
  pow(convertedGivenMat, gamma, poweredMat);
  poweredMat *= 255.0;

  return A*poweredMat;
}

int main() {
  Mat image(285, 778, CV_8U, Scalar(0));

  int wid = 768;
  int hei = 60;

  Rect roiLongGradient(5, 5, wid, hei);
  Mat matLongGradient = image(roiLongGradient);
  PourWithGradient(matLongGradient, 3);

  Rect roiGamma(5, 65, wid, hei);
  Mat matGamma = image(roiGamma);
  matLongGradient.copyTo(matGamma);
  CorrectGammaByMatrix(matGamma, 2).convertTo(matGamma, image.type());

  Rect roiStepGradient(5, 160, wid, hei);
  Mat matStepGradient = image(roiStepGradient);
  PourWithGradient(matStepGradient, 30, 10);
  for (int i = matStepGradient.cols-1; i > matStepGradient.cols-20; i--)
    matStepGradient.col(i).setTo(255);

  Rect roiStepGamma(5, 220, wid, hei);
  Mat matStepGamma = image(roiStepGamma);
  matStepGradient.copyTo(matStepGamma);
  for (int i = matStepGamma.cols - 1; i > matStepGamma.cols - 20; i--)
    matStepGamma.col(i).setTo(255);
  CorrectGammaByMatrix(matStepGamma, 2.4).convertTo(matStepGamma, image.type());

  namedWindow("Gradient", CV_WINDOW_NORMAL);
  imshow("Gradient", image);
  waitKey(0);
}

```