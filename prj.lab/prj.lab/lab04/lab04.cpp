#include <opencv2\opencv.hpp>
#include <iostream>
#include <string>


using namespace cv;
using namespace std;


void drawFigures(Mat& background) {
  int rowsNumber = 2;
  int columnsNumber = 3;
  int height = 200;
  int width = 200;

  int backsColors[] = { 0, 127, 255 };
  int circlesColors[] = { 0, 127, 255 };

  Rect rec = Rect(0, 0, width, height);
  rectangle(background, rec, Scalar(255), -1, 8, 0);
  circle(background, cv::Point(100, 100), 40, Scalar(0), -1);

  rec = Rect(width, 0, width, height);
  rectangle(background, rec, Scalar(0), -1, 8, 0);
  circle(background, cv::Point(300, 100), 40, Scalar(127), -1);

  rec = Rect(2 * width, 0, width, height);
  rectangle(background, rec, Scalar(127), -1, 8, 0);
  circle(background, cv::Point(500, 100), 40, Scalar(255), -1);

  rec = Rect(0, height, width, height);
  rectangle(background, rec, Scalar(127), -1, 8, 0);
  circle(background, cv::Point(100, 300), 40, Scalar(255), -1);

  rec = Rect(width, height, width, height);
  rectangle(background, rec, Scalar(255), -1, 8, 0);
  circle(background, cv::Point(300, 300), 40, Scalar(0), -1);

  rec = Rect(2 * width, height, width, height);
  rectangle(background, rec, Scalar(0), -1, 8, 0);
  circle(background, cv::Point(500, 300), 40, Scalar(127), -1);
}

void filtrate(Mat& src, Mat& dst, Mat& filter) {
  src.convertTo(dst, CV_32F);
  filter2D(dst, dst, -1, filter);
}

void showFiltration(Mat& src, int num) {
  Mat showing;
  src.copyTo(showing);

  showing = (showing + 255) / 2;
  showing.convertTo(showing, CV_8U);

  imshow("Filter" + to_string(num), showing);
}


int main() {
  Mat background(400, 600, CV_8U, Scalar(0));

  drawFigures(background);
  //imshow("Source", background);

  Mat filter1 = (Mat_<double>(2, 2) << 1, 0, 0, -1);
  Mat filter1Result;
  filtrate(background, filter1Result, filter1);
  showFiltration(filter1Result, 1);

  Mat filter2 = (Mat_<double>(2, 2) << 0, 1, -1, 0);
  Mat filter2Result;
  filtrate(background, filter2Result, filter2);
  showFiltration(filter2Result, 2);

  Mat filter3Result;
  pow(filter1Result, 2, filter1Result);
  pow(filter2Result, 2, filter2Result);
  sqrt((filter1Result + filter2Result), filter3Result);
  showFiltration(filter3Result, 3);

  waitKey();
}

