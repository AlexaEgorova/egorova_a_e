#include <opencv2\opencv.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace cv;
using namespace std;


Mat ReadMat(const string& path) {
  Mat img = imread(path);
  if (img.empty())
    cout << "Cannot read img " << path << endl;
  return img;
}

Mat GammaCorrection(Mat& givenMat, double gamma, double A = 1.0) {
  Mat convertedGivenMat;
  givenMat.convertTo(convertedGivenMat, CV_32F);
  convertedGivenMat /= 255.0;

  Mat poweredMat;
  pow(convertedGivenMat, gamma, poweredMat);
  poweredMat *= 255.0;
  return A*poweredMat;
}

string CompressToJpg(const Mat& src, const string& srcName, int compression) {
  std::vector<int> params;
  params.push_back(CV_IMWRITE_JPEG_QUALITY);
  params.push_back(compression);
  string name = srcName + to_string(compression) + ".jpg";
  imwrite(name, src, params);
  cout << "Saved " << name << endl;
  return name;
}

int main() {
  Mat img, img95, img65;

  // ����������� � ������
  string path = "D:/uch/egorova_a_e/testdata/cross_0256x0256.png";
  ReadMat(path).copyTo(img);
  string name = CompressToJpg(img, "cross", 95);
  ReadMat(name).copyTo(img95);
  name = CompressToJpg(img, "cross", 65);
  ReadMat(name).copyTo(img65);

  // �������� ��� ��������� ����������
  Mat background(img.rows * 3, img.cols * 5, CV_8UC3, Scalar(0));

  vector<Mat> channels(3);
  int rowsNumber = 3;
  int channelsNumber = 3;
  int height = img.size[0];
  int width = img.size[1];
  Mat matsPerRow[] = { img, img95, img65 };

  for (int row = 0; row < rowsNumber; ++row) {
    split(matsPerRow[row], channels);
    Mat empty_image = Mat::zeros(channels[0].rows, channels[0].cols, CV_8UC1);
    Mat distr[3] = { empty_image, empty_image, empty_image };

    // ����������� ��������
    for (int column = 0; column < channelsNumber; ++column) {
      Mat result;
      distr[column] = channels[column];
      merge(distr, 3, result);

      if (row > 0) {
        Mat prevMat = background(Rect(column*width, 0, width, height));
        subtract(result, prevMat, result);
        vector<Mat> channels1(3);
        Mat sub_distr[3] = { empty_image, empty_image, empty_image };
        split(result, channels1);
        // �����-��������� ��� ���������������
        GammaCorrection(channels1[column], 2.2).convertTo(channels1[column], CV_8UC1, 255);
        sub_distr[column] = channels1[column];
        merge(sub_distr, 3, result);
        //cv::pow(result, 2, result);
      }
      Mat matRoi = background(Rect(column*width, row*height, width, height));
      result.copyTo(matRoi);
      distr[column] = empty_image;
    }

    // ��������� ��������
    Mat grayImg;
    cvtColor(matsPerRow[row], grayImg, CV_RGB2GRAY);
    Mat bw_distr[3] = { grayImg, grayImg, grayImg };
    if (row == 0) {
      merge(bw_distr, 3, grayImg);
    }
    else {
      Mat prevMat = background(Rect(channelsNumber*width, 0, width, height));
      vector<Mat> channels(3);
      split(prevMat, channels);
      subtract(grayImg, channels[0], grayImg);
      Mat bw_sub_distr[3] = { grayImg, grayImg, grayImg };
      for (auto& b_s_d : bw_sub_distr)
        GammaCorrection(b_s_d, 2.2).convertTo(b_s_d, CV_8UC1, 255);
      merge(bw_sub_distr, 3, grayImg);
      //cv::pow(grayImg, 2, grayImg);
    }
    Mat matRoi = background(Rect(channelsNumber*width, row*height, width, height));
    grayImg.copyTo(matRoi);

    Mat matRoiSrc = background(Rect((channelsNumber+1)*width, row*height, width, height));
    matsPerRow[row].copyTo(matRoiSrc);
  }

  // ��������������� ��������� �����������
  resize(background, background, Size(1100, background.rows * 1100 / background.cols));
  imshow("Compression&Channels", background);

  waitKey();
}

