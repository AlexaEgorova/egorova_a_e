#include <opencv2\opencv.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace cv;
using namespace std;


void buildHist(const Mat& src, Mat& dst, bool needSave = false) {
  int histSize = 256;
  float range[] = { 0, histSize };
  const float* histRange = { range };

  Mat hist;
  calcHist(&src, 1, 0, Mat(), hist, 1, &histSize, &histRange);

  int histWidth = 512;
  int histHeight = 300;
  int step = cvRound((double)histWidth / histSize);

  Mat histMat(histHeight, histWidth, CV_8U, Scalar(255));
  normalize(hist, hist, 0, histMat.rows, NORM_MINMAX, -1, Mat());

  for (int i = 1; i < histSize; i++) {
    auto start = Point(step*(i - 1), histHeight - cvRound(hist.at<float>(i - 1)));
    auto rightEnd = Point(step*(i), histHeight - cvRound(hist.at<float>(i)));
    auto bottomEnd = Point(step*(i - 1), histHeight);
    line(histMat, start, rightEnd, Scalar(0)); // contour
    line(histMat, start, bottomEnd, Scalar(0), step); // fill
  }
  if (needSave)
    imwrite("hist.png", histMat);
  histMat.copyTo(dst);
}

int getThreshold(Mat& src) {
  int thres = 0;
  int histSize = 256;
  float range[] = { 0, histSize };
  const float* histRange = { range };

  Mat hist;
  calcHist(&src, 1, 0, Mat(), hist, 1, &histSize, &histRange);

  int histWidth = 512;
  int histHeight = 300;
  int step = cvRound((double)histWidth / histSize);

  Mat histMat(histHeight, histWidth, CV_8U, Scalar(255));

  int src_size = src.cols * src.rows;
  int sum_thres = src_size / 2;
  int sum = 0;

  for (int i = 1; i < histSize; i++) {
    if (sum < sum_thres && sum + hist.at<float>(i) > sum_thres)
      thres = i;
    sum += hist.at<float>(i);
  }

  return thres;
}

void showGraph(const double& alpha, const int& beta) {
  Mat funcMat(256, 256, CV_8UC1, Scalar(200));
  for (int i = 1; i < 256; i++) {
    auto start = Point(i - 1, alpha*(i - 1) + beta);
    auto end = Point(i, alpha*i + beta);
    line(funcMat, start, end, Scalar(0), 2);
    cout << start << "  " << end << endl;
  }
  imshow("Function", funcMat);

}

void correctImg(const Mat& src, Mat& dst) {
  srand(time(NULL));
  // 1.0-3.0 & -100..100
  //int beta = (rand() % 100 + 1) * pow(-1, rand());
  //int beta = pow(-1, rand());
  // but it is too unpredictable in terms of result
  // let's do alpha 1.0-1.5 and beta -1..1
  double alpha = double(rand() % 5 + 10) / 10;
  int beta = -(rand() % 50 + 1);
  showGraph(alpha, beta);

  cout << alpha << "  " << beta << endl;

  Mat correctedImg;
  src.copyTo(correctedImg);
  correctedImg.convertTo(correctedImg, CV_32F);
  correctedImg *= alpha;
  correctedImg += beta;
  correctedImg.convertTo(correctedImg, CV_8U);

  correctedImg.copyTo(dst);
}

void binarizeGlobally(const Mat& src, Mat& dst, int threshold) {
  src.copyTo(dst);
  for (int i = 0; i < dst.cols; i++) {
    for (int j = 0; j < dst.rows; j++) {
      auto color = dst.at<unsigned char>(j, i);
      auto newColor = color < threshold ? 0 : 255;
      dst.at<unsigned char>(j, i) = newColor;
    }
  }
}

void binarizeLocally(unsigned char* src, unsigned char* res, int width, int height) {
  const int S = width / 8;
  int s2 = S / 2;
  const float t = 0.15;
  unsigned long* integral_image = 0;
  long sum = 0;
  int count = 0;
  int index;
  int x1, y1, x2, y2;

  integral_image = new unsigned long[width*height * sizeof(unsigned long*)];

  for (int i = 0; i < width; i++) {
    sum = 0;
    for (int j = 0; j < height; j++) {
      index = j * width + i;
      sum += src[index];
      if (i == 0)
        integral_image[index] = sum;
      else
        integral_image[index] = integral_image[index - 1] + sum;
    }
  }

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      index = j * width + i;

      x1 = i - s2;
      x2 = i + s2;
      y1 = j - s2;
      y2 = j + s2;

      if (x1 < 0)
        x1 = 0;
      if (x2 >= width)
        x2 = width - 1;
      if (y1 < 0)
        y1 = 0;
      if (y2 >= height)
        y2 = height - 1;

      count = (x2 - x1)*(y2 - y1);

      sum = integral_image[y2*width + x2] - integral_image[y1*width + x2] -
        integral_image[y2*width + x1] + integral_image[y1*width + x1];
      if ((long)(src[index] * count) < (long)(sum*(1.0 - t)))
        res[index] = 0;
      else
        res[index] = 255;
    }
  }

  delete[] integral_image;
}

void filtrate(Mat& src, Mat& dst, Mat& filter) {
  src.convertTo(dst, CV_32F);
  filter2D(dst, dst, -1, filter);
}

void showFiltration(Mat& src, int num) {
  Mat showing;
  src.copyTo(showing);

  showing = (showing + 255) / 2;
  showing.convertTo(showing, CV_8U);

  imshow("Filter" + to_string(num), showing);
}


// i,j of right bottom inner corner are given
bool toColorRounded(Mat& img, int filt_is, int filt_js, int inner_is, int inner_js, int i, int j) {
  bool monorounded = false;
  int blackCount = 0;
  int whiteCount = 0;
  int filter_size = filt_is * filt_js;
  int in_size = inner_is * inner_js;

  for (int k = i - inner_is + 1 - (filt_is - inner_is) / 2; k < i + (filt_is - inner_is) / 2 + 1; ++k) {
    for (int t = j - inner_js + 1 - (filt_js - inner_js) / 2; t < j + (filt_js - inner_js) / 2 + 1; ++t) {
      int color = img.at<unsigned char>(t, k);
      if (color == 0)
        ++blackCount;
      if (color == 255)
        ++whiteCount;
    }
  }
  int count = blackCount + whiteCount;
  if ((double)blackCount / count > 0.67) {
    img.at<unsigned char>(j, i) = 0;
    img.at<unsigned char>(j, i-1) = 0;
    img.at<unsigned char>(j-1, i) = 0;
    img.at<unsigned char>(j-1, i-1) = 0;

  }
  if ((double)whiteCount / count >  0.67) {
    img.at<unsigned char>(j, i) = 255;
    img.at<unsigned char>(j, i - 1) = 255;
    img.at<unsigned char>(j - 1, i) = 255;
    img.at<unsigned char>(j - 1, i - 1) = 255;
  }

  return true;
}


void getStartEnd(int& start_i, int& end_i, int& start_j, int& end_j,
                 int& filter_width, int& filter_height,
                 int& inner_width, int& inner_height,
                 int& dstcols, int& dstrows) {

  start_i = 0;
  start_j = 0;
  end_i = 0;
  end_j = 0;

  start_i = inner_width + (filter_width - inner_width) / 2 - 1;
  end_i = dstcols - (filter_width - inner_width) / 2;

  start_j = inner_height + (filter_height - inner_height) / 2 - 1;
  end_j = dstrows - (filter_height - inner_height) / 2;
}

void smooth(Mat& src, Mat& dst, int threshold) {
  src.copyTo(dst);
  int filter_width = 7;
  int filter_height = 7;
  int inner_width = 3;
  int inner_height = 3;

  int start_i;
  int start_j;
  int end_i;
  int end_j;

  getStartEnd(start_i, end_i, start_j, end_j,
    filter_width, filter_height,
    inner_width, inner_height,
    dst.cols, dst.rows);

  for (int i = start_i; i < end_i; i++) {
    for (int j = start_j; j < end_j; j++) {
      toColorRounded(dst, filter_width, filter_height, inner_width, inner_height, i, j);
    }
  }

  imshow("premonoed", dst);

  filter_width = 15;
  filter_height = 15;
  inner_width = 5;
  inner_height = 5;

  getStartEnd(start_i, end_i, start_j, end_j,
    filter_width, filter_height,
    inner_width, inner_height,
    dst.cols, dst.rows);

  for (int i = start_i; i < end_i; i++) {
    for (int j = start_j; j < end_j; j++) {
      toColorRounded(dst, filter_width, filter_height, inner_width, inner_height, i, j);
    }
  }


}


int main(int argc, char** argv) {
  Mat src = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
  if (!src.data) {
    cout << "Error reading " << argv[1] << endl;
    waitKey(0);
    return -1;
  }

  Mat background(src.rows * 2 + 256, src.cols * 2 + 256, CV_8UC1, Scalar(60));
  Mat matRoi = background(Rect(0, 0, src.cols, src.rows));
  src.copyTo(matRoi);

  Mat srcHist;
  int delay = 50;
  buildHist(src, srcHist, true);
  matRoi = background(Rect(src.cols + delay,
    (src.rows - srcHist.rows) / 2,
    srcHist.cols,
    srcHist.rows));
  srcHist.copyTo(matRoi);

  Mat correctedImg;
  correctImg(src, correctedImg);
  matRoi = background(Rect(0, src.rows, src.cols, src.rows));
  correctedImg.copyTo(matRoi);

  Mat correctedHist;
  buildHist(correctedImg, correctedHist);
  matRoi = background(Rect(correctedImg.cols + delay,
    src.rows + (correctedImg.rows + 2 - correctedHist.rows) / 2,
    correctedHist.cols,
    correctedHist.rows));
  correctedHist.copyTo(matRoi);

  Mat slahes(max(src.rows * 2, srcHist.rows * 2) + delay * 2,
    max(srcHist.cols * 3, 3 * src.cols) + delay * 3,
    CV_8UC1,
    Scalar(60));

  auto ahe = createCLAHE();

  src.copyTo(correctedImg);
  Mat slahe1;
  ahe->apply(correctedImg, slahe1);
  auto clipLimit = ahe->getClipLimit();
  auto gridSize = ahe->getTilesGridSize();
  matRoi = slahes(Rect(0, 0, slahe1.cols, slahe1.rows));
  slahe1.copyTo(matRoi);
  buildHist(slahe1, srcHist, true);
  matRoi = slahes(Rect(0,
    slahe1.rows + delay,
    srcHist.cols,
    srcHist.rows));
  srcHist.copyTo(matRoi);

  Mat slahe2;
  ahe->setClipLimit(ahe->getClipLimit() / 5);
  ahe->apply(correctedImg, slahe2);
  clipLimit = ahe->getClipLimit();
  gridSize = ahe->getTilesGridSize();
  matRoi = slahes(Rect(srcHist.cols + delay, 0, slahe2.cols, slahe2.rows));
  slahe2.copyTo(matRoi);
  buildHist(slahe2, srcHist, true);
  matRoi = slahes(Rect(srcHist.cols + delay,
    slahe2.rows + delay,
    srcHist.cols,
    srcHist.rows));
  srcHist.copyTo(matRoi);

  Mat slahe3;
  ahe->setTilesGridSize(ahe->getTilesGridSize() / 3);
  ahe->apply(correctedImg, slahe3);
  clipLimit = ahe->getClipLimit();
  gridSize = ahe->getTilesGridSize();
  matRoi = slahes(Rect((srcHist.cols + delay) * 2,
    0,
    slahe3.cols,
    slahe3.rows));
  slahe3.copyTo(matRoi);

  buildHist(slahe3, srcHist, true);
  matRoi = slahes(Rect((srcHist.cols + delay) * 2,
    slahe3.rows + delay,
    srcHist.cols,
    srcHist.rows));
  srcHist.copyTo(matRoi);

  resize(slahes, slahes, Size(1200, slahes.rows * 1200 / slahes.cols));
  imshow("slahes", slahes);

  //resize(background, background, Size(700, background.rows * 700 / background.cols));
  imshow("Change Func", background);

  Mat globaled;
  src.copyTo(globaled);
  int threshold = getThreshold(globaled);
  binarizeGlobally(globaled, globaled, threshold);
  imshow("Global binarization", globaled);

  Mat local;
  src.copyTo(local);
  uchar *bla = local.data;
  binarizeLocally(bla, bla, src.cols, src.rows);
  imshow("Local", local);

  Mat filterResult;
  smooth(globaled, filterResult, threshold);

  for (int i = 0; i < filterResult.cols; i++) {
    for (int j = 0; j < filterResult.rows; j++) {
      int color = filterResult.at<unsigned char>(j, i);
      int new_color = color > 0 ? 255 : 0;
      filterResult.at<unsigned char>(j, i) = new_color;
    }
  }
  imshow("filter bined", filterResult);

  Mat masked;
  src.copyTo(masked, filterResult);
  imshow("result", masked);

  Mat invertMasked;
  filterResult = 255 - filterResult;
  src.copyTo(invertMasked, filterResult);
  imshow("inverted", invertMasked);

  waitKey(0);
  return 0;

}

